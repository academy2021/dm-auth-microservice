package com.controllers;

import com.entities.UserEntity;
import com.services.UserService;
import com.dto.UserResponseDTO;
import com.services.tokens.JwtTokenProvider;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.websocket.server.PathParam;

@RestController
@RequestMapping("/auth")
@CrossOrigin(origins = "*")
public class UserController {

    @Autowired
    private UserService userService;
    @Autowired
    private ModelMapper modelMapper;
    @Autowired
    private JwtTokenProvider jwtTokenProvider;
    @PostMapping("/signin")
    public String login(@RequestBody UserEntity user) {
        return userService.signin(user.getUsername(), user.getPassword());
    }

    @PostMapping("/signup")
    public String signUp(@RequestBody UserEntity user) {
        return userService.signup(user);
    }

    @GetMapping(value = "/getuser")
    public UserResponseDTO getUser(HttpServletRequest req) {
        return modelMapper.map(userService.getUser(req), UserResponseDTO.class);
    }

    @GetMapping(value = "/validateToken")
    public boolean validateToken(@RequestParam String token) {
        return jwtTokenProvider.validateToken(token);

    }




}
